# -*- coding: utf-8 -*-

import csv
import codecs

if __name__ == '__main__':

  d = {'11':'Еврейская АО',
   '14':'Иркутская область',
   '18':'Камчатский край',
   '28':'Магаданская область',
   '40':'Приморский край',
   '45':'Республика Бурятия',
   '64':'Сахалинская область'}

  res = open('res.csv', 'w')
  writer = csv.writer(res)

  with open('./output.csv') as csvfile:
      match = csv.reader(csvfile, delimiter=';')
      output = list(match)

  with open('./SPN.csv') as spnfile:
      match = csv.reader(spnfile, delimiter='|')
      spn = list(match)

  for i in spn:
      for j in output:
          if (len(i) == 3):
            if(i[0].strip() == j[0].strip()):
                temp = j[1]
                i.append(d.get(temp))
                # print(i)
                writer.writerow(i)

  res.close()
